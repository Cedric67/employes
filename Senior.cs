﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstProject
{
    public class Senior : Employe
    {
        public struct Indices
        {
            public const int _1 = 13000;
            public const int _2 = 15000;
            public const int _3 = 17000;
            public const int _4 = 20000;
        }

        public Senior(int indice, int regNumber, string firstname, string lastname, DateTime birthdate) : base(regNumber, firstname, lastname, birthdate)
        {
            Indice = indice;
        }

        private int Indice { get; set; }

        public override string ToString()
        {
            return "{\"Employe\":" + base.ToString() + ", \"Indice\":\"" + Indice + "\",\"Salary\":" + GetSalary() + "€\"}";
        }

        public override double GetSalary()
        {
            return Indice;
        }
    }
}
