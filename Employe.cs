﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstProject
{
    public abstract class Employe
    {
        protected Employe(int regNumber, string firstname, string lastname, DateTime birthdate)
        {
            RegNumber = regNumber;
            Firstname = firstname;
            Lastname = lastname;
            Birthdate = birthdate;
        }

        protected int RegNumber { get; set; }

        protected string Firstname { get; set; }

        protected string Lastname { get; set; }

        protected DateTime Birthdate { get; set; }

        public override string ToString()
        {
            return "{\"Firstname\":\"" +Firstname + "\",\"Lastname\":\"" + Lastname + "\",\"Birthdate\":\"" + Birthdate.ToShortDateString() + "\"}";
        }

        public abstract double GetSalary();
    }
}
