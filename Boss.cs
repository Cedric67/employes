﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstProject
{
    public class Boss : Employe
    {
        public Boss(float ca, double percent, int regNumber, string firstname, string lastname, DateTime birthdate) : base(regNumber, firstname, lastname, birthdate)
        {
            CA = ca;
            Percent = percent;
        }

        private float CA { get; set; }

        private double Percent { get; set; }

        public override string ToString()
        {
            return "{\"Employe\":" + base.ToString() + ",\"CA\":\"" + CA + "\",\"Percent\":\"" + Percent + "\",\"Salary\":\"" +GetSalary() + "€\"}";
        }

        public override double GetSalary()
        {
            return CA * Percent / 100;
        }
    }
}
