﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstProject
{
    class Program
    {
        static void Main(string[] args)
        {
            Worker worker1 = new Worker(1, "Adam", "SMITH", DateTime.Parse("1970-06-15"));
            Senior senior1 = new Senior(Senior.Indices._1, 1, "Eve", "SMITH", DateTime.Parse("1975-03-20"));
            Boss boss1 = new Boss(100009, 13.73, 1, "Jack", "JONES", DateTime.Parse("1950-01-11"));

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.WriteLine(worker1);
            Console.WriteLine(senior1);
            Console.WriteLine(boss1);

            Console.ReadLine();
        }
    }
}
