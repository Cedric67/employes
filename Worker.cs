﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstProject
{
    public class Worker : Employe
    {
        private const float SMIG = 2500;

        public Worker(int regNumber, string firstname, string lastname, DateTime birthdate) : base(regNumber, firstname, lastname, birthdate)
        {
        }

        public override string ToString()
        {
            return "{\"Employe\":" + base.ToString() + ",\"Salary\":\"" + GetSalary() + "€\"}";
        }
        public override double GetSalary()
        {
            return SMIG * Birthdate.Year * 100 > SMIG * 2 ? SMIG * 2 : SMIG * Birthdate.Year * 100;
        }
    }
}
